module.exports = function keys(dataObj) {
  if (arguments.length === 0) return [];

  if(typeof dataObj !== "object") return [];
  
  const keysArr = [];
  
  for (let key in dataObj) {
    keysArr.push(key);
  }

  return keysArr;
};
