//Importing test object
const testObject = require("./objects.cjs");

//Importing the keys function
const keyFunc = require("./keys.cjs");
//Storing the key array
const allKeys = keyFunc(testObject);

//Consoling the output
console.log(allKeys);
