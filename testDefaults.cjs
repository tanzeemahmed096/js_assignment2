//Importing test object
const testObject = require("./objects.cjs");

//Importing the defaultObj function
const defaultObjFunc = require("./defaults.cjs");
//Storing the key array
const defaultObj = defaultObjFunc(testObject);

//Consoling the output
console.log(defaultObj);
