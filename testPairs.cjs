//Importing test object
const testObject = require("./objects.cjs");

//Importing the pair function
const pairFunc = require("./pairs.cjs");
//Storing the key array
const allPairs = pairFunc(testObject);

//Consoling the output
console.log(allPairs);
