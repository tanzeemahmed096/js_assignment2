//Creating map object function
module.exports = function mapObject(dataObj, keyValues) {
  //if the agrument is not an object or no parameter given the return empty array
  if (
    arguments.length === 0 ||
    JSON.stringify(dataObj) === "{}" ||
    typeof dataObj !== "object" ||
    (Array.isArray(dataObj) && typeof dataObj[0] !== "object")
  ) {
    console.log("Provide valid data");
    return {};
  }

  //If callback is not provided then return empty object
  if(keyValues === null){
    console.log("Callback not provided");
    return {};
  }

  //Empty obj to add the modified values
  const modifiedObj = {};

  // Iteraring over the modified object and updating using keyValues function
  for (let key in dataObj) {
    modifiedObj[key] = keyValues(dataObj[key], key);
  }

  //Returning modified object
  return modifiedObj;
};
