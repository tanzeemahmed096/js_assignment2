//Creating values function which will return values of the properties
module.exports = function values(dataObj) {
  //if the agrument is not an object or no parameter given the return empty array
  if (arguments.length === 0 || JSON.stringify(dataObj) === "{}" || typeof dataObj !== "object") {
    console.log("Provide valid data");
    return [];
  }

  //If the given parameter is and array the return elements as values
  if(Array.isArray(dataObj)){
    //Return keys Array as the elements are values
    return keysArr;
  }
  
  const valuesArr = [];

  //Iterating over object to get the value
  for (let key in dataObj) {
    //If the key is of function type then ignoring that key value pair
    if (typeof key === "function") continue;
    else valuesArr.push(dataObj[key]);
  }

  //Return the array consisting of all the values
  return valuesArr;
};
