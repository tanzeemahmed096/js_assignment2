//Importing test object
const testObject = require("./objects.cjs");

//Importing the values funtion
const valueFunc = require("./values.cjs");
//Storing the values array in a varialble
const allValues = valueFunc(testObject);

//Consoling output
console.log(allValues);