//Creating invert funtion
module.exports = function invert(dataObj) {
  //if the agrument is not an object or no parameter given the return empty array
  if (
    arguments.length === 0 ||
    JSON.stringify(dataObj) === "{}" ||
    typeof dataObj !== "object" ||
    (Array.isArray(dataObj) && typeof dataObj[0] !== "object")
  ) {
    console.log("Provide valid data");
    return {};
  }

  //Creating an object that will return invert of original object
  const invertObj = {};
  for (let key in dataObj) {
    //Since the values are assumed to be string and unique
    invertObj[dataObj[key]] = key;
  }

  //returning the inverted object
  return invertObj;
};
