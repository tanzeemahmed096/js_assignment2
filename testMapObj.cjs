//Importing test object
const testObject = require("./objects.cjs");

//Importing mapObj function
const mapObjFn = require("./mapObject.cjs");

//Storing the updated object in a variable
const mapObj = mapObjFn(testObject, keyValues);

function keyValues(val, key) {
  return val + 100;
}

//Consoling the output
console.log(mapObj);
