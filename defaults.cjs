//Creating default function that will return the object containing default values
module.exports = function defaultFunc(...agrs) {
  //Checking if there is single object then return that object
  if (arguments.length === 1) return arguments[0];
  const dataObj = arguments[0];

  //Iterating over the arguments array from index 1 as index 0 will be considered as default object
  for (let idx = 1; idx < arguments.length; idx++) {
    //Storing the arguments object to loop over that indexed object
    const getArgObj = arguments[idx];
    for (let key in getArgObj) {
      //checking if the key exist in default object already
      if (!checkKey(dataObj, key)) {
        //Adding property and value if the property is not present in default object
        dataObj[key] = getArgObj[key];
      }
    }
  }

  //Returning default object
  return dataObj;
};

//Iterating over the object to check the key is present in the default object or not
function checkKey(dataObj, val) {
  for (let key in dataObj) {
    if (dataObj[key] === val) return true;
  }

  return false;
}