//Importing test object
const testObject = require("./objects.cjs");

//Importing the invert function
const invertFunc = require('./invert.cjs')
const invertObj = invertFunc(testObject)

//Consoling the output
console.log(invertObj);