//Creating a pair function which will return a pair of list
module.exports = function pairs(dataObj) {
  //if the agrument is not an object or no parameter given the return empty array
  if (
    arguments.length === 0 ||
    JSON.stringify(dataObj) === "{}" ||
    typeof dataObj !== "object" ||
    (Array.isArray(dataObj) && typeof dataObj[0] !== "object")
  ) {
    console.log("Provide valid data");
    return [];
  }
  
  const resPair = [];

  //Iterating over the object to get keys and values
  for (let key in dataObj) {
    // creating a array which will be pushed to list array
    const arr = [];
    arr[0] = key;
    arr[1] = dataObj[key];
    resPair.push(arr);
  }

  //Returning the list
  return resPair;
};
